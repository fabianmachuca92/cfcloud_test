﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CFCLOUD_test._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Algoritmo de busqueda</h1>
        <p class="lead">Ingrese el usuario 1 y el usuario 2, el sistema le mostrará si ambos usuarios tienen alguna relacion y la distancia entre ellos.</p>

    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Usuario 1</h2>
            <asp:TextBox runat="server" ID="txt_usuario1" ClientIDMode="Static" CssClass="form-control" required></asp:TextBox>
        </div>
        <div class="col-md-4">
            <h2>Usuario 2</h2>
            <asp:TextBox runat="server" ID="txt_usuario2" ClientIDMode="Static" CssClass="form-control" required></asp:TextBox>


        </div>
        <div class="col-md-4">
            <h2>Resultado</h2>
            <p runat="server" id="resultado">
               
            </p>
            <p runat="server" id="distancia">
                
            </p>
            <asp:Button runat="server" ID="btn_buscar" Text="buscar" OnClick="btn_buscar_Click" OnClientClick="validate();" CssClass="btn btn-primary" />
        </div>
    </div>
    <script>
        function validate() {
            $('#txt_usuario1').val($('#txt_usuario1').val().trim())
            $('#txt_usuario2').val($('#txt_usuario2').val().trim())
        }
    </script>
</asp:Content>
