﻿using CFCLOUD_test.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CFCLOUD_test.Controllers
{
    public class FollowersController : ApiController
    {

        //// GET: api/Followers
        [Route("api/Followers")]
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(bd._users.Select(a => new Followers()
                {
                    user = a.user,
                    followers = a.followers
                }).ToList());
            }
            catch (Exception)
            {
                return NotFound();
            }

        }

        // GET: api/Followers/5
        public IHttpActionResult Get(string id)
        {
            try
            {
                Followers _followers = new Followers() { user = id, followers = new string[0] };
                var _user = bd._users.Where(a => a.user == id).FirstOrDefault();
                if (_user != null)
                    _followers.followers = _user.followers;

                return Ok(_followers);
            }
            catch (Exception)
            {
                return NotFound();
            }

        }



    }
}
