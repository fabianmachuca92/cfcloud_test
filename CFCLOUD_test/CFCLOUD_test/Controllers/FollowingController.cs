﻿using CFCLOUD_test.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CFCLOUD_test.Controllers
{
    public class FollowingController : ApiController
    {

        [Route("api/Following")]
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(bd._users.Select(a => new Following()
                {
                    user = a.user,
                    following = a.following
                }).ToList());
            }
            catch (Exception)
            {
                return NotFound();
            }

        }

        // GET: api/Following/5
        public IHttpActionResult Get(string id)
        {
            try
            {
                Following _following = new Following() { user = id, following = new string[0] };
                var _user = bd._users.Where(a => a.user == id).FirstOrDefault();
                if (_user != null)
                    _following.following = _user.following;
                return Ok(_following);


            }
            catch (Exception)
            {
                return NotFound();
            }
        }


    }
}
