﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CFCLOUD_test.Models
{
    public class bd
    {
        public static List<users> _users = new List<users>() {
            new users()
            {
                user = "fabian",
                followers = new string[] { "jose", "carlos", "andres" },
                following = new string[] { "ana", "maria", "jose" }
            } ,
            new users()
            {
                user = "yanira",
                followers = new string[] { "ernesto", "ana", "jose" },
                following = new string[] { "carlos", "fabian", "nancy" }
            },
            new users()
            {
                user = "jose",
                followers = new string[] { "josue", "pedro", "pablo" },
                following = new string[] { "andres", "maria", "pepe" }
            },
            new users()
            {
                user = "luis",
                followers = new string[] {  },
                following = new string[] { "ana", "yanira", "pablo" }
            },
            new users()
            {
                user = "ernesto",
                followers = new string[] { "fabian","miguel" },
                following = new string[] { "andres",  "mike" }
            }
        };
    }

    public class users
    {
        public string user { get; set; }
        public string[] followers { get; set; }
        public string[] following { get; set; }
    }
}