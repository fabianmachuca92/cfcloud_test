﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CFCLOUD_test.Models
{
    public class Following
    {
        public string user { get; set; }
        public string[] following { get; set; }
    }
}