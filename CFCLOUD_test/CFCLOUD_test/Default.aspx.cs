﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CFCLOUD_test.Models;

namespace CFCLOUD_test
{
    public partial class _Default : Page
    {


        public List<string> UsuariosExaminados = new List<string>();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_buscar_Click(object sender, EventArgs e)
        {
            try
            {
                resultado.InnerText = string.Empty;
                if (!string.IsNullOrEmpty(txt_usuario1.Text) && !string.IsNullOrEmpty(txt_usuario2.Text))
                    Buscar(txt_usuario1.Text.Trim(), txt_usuario2.Text.Trim());
                else
                    resultado.InnerText = "Ingrese todos los campos requeridos";
            }
            catch (Exception ex)
            {
                resultado.InnerText = ex.Message;
            }
        }


        protected void Buscar(string user1, string user2)
        {
            int MaxLevel = 5;
            int disctance;
            bool bdFound = false;
            string txWay = string.Empty;

            ///Se busca en los usuarios que siguen y esta siguiendo el usuario 1
            if (FindFollowersFollowing(user1, user2, MaxLevel, out disctance, out txWay))
                bdFound = true;
            else
            {
                ///Se busca en los usuarios que siguen y esta siguiendo el usuario 2
                bdFound = FindFollowersFollowing(user2, user1, MaxLevel, out disctance, out txWay);

            }

            if (bdFound)
                resultado.InnerHtml = string.Format("Se encontro la distancia de {0} usuario(s) entre {1} y {2} <br> {3}", disctance, user1, user2, txWay);
            else
                resultado.InnerHtml = string.Format("No se encontro relación entre el usuario {0} y {1}", disctance, user1, user2);
        }

        ///Este método busca en ambos sentidos (siguiendo / seguidores) a los usarios hasta el nivel maximo seteado
        private bool FindFollowersFollowing(string user1, string user2, int maxLevel, out int disctance, out string txUsuarioRelacion, int? currentLevel = 0)
        {
            txUsuarioRelacion = string.Empty;
            disctance = 0;
            #region Se busca en los seguidores

            var _followers = GetFollowers(user1).Where(a => !UsuariosExaminados.Contains(a)).ToList();
            bool found = _followers.Any(a => a == user2);
            if (!found && currentLevel < maxLevel)
            {
                foreach (var _relacion in _followers)
                {
                    if (FindFollowersFollowing(_relacion, user2, maxLevel, out disctance, out txUsuarioRelacion, currentLevel++))
                    {
                        disctance++;
                        txUsuarioRelacion = string.Format("{0} follower > {1}", user1, txUsuarioRelacion);
                        return true;
                    }
                    //else UsuariosExaminados.Add(_relacion);
                }
                
                found = false;
            }

            #endregion

            if (found)
            {
                txUsuarioRelacion = string.Format("{0} follower > {1}", user1, user2);
                return true;
            }

            #region Se busca en los que sigue

            var _following = GetFollowing(user1).Where(a => !UsuariosExaminados.Contains(a)).ToList();
            found = _following.Any(a => a == user2);
            if (!found && currentLevel < maxLevel)
            {
                foreach (var _relacion in _following)
                {
                    if (FindFollowersFollowing(_relacion, user2, maxLevel, out disctance, out txUsuarioRelacion, currentLevel++))
                    {
                        disctance++;
                        txUsuarioRelacion = string.Format("{0} following > {1}", user1, txUsuarioRelacion);
                        return true;
                    }
                    //else UsuariosExaminados.Add(_relacion);
                }
                
                found = false;
            }
            else
            {
                if (found)
                    txUsuarioRelacion = string.Format("{0} following > {1}", user1, user2);
            }

            #endregion

            return found;
        }


        private string[] GetFollowers(string user)
        {
            var _user = bd._users.Where(a => a.user == user).FirstOrDefault();
            if (_user != null)
                return _user.followers;
            else
                return new string[0];
        }

        private string[] GetFollowing(string user)
        {
            var _user = bd._users.Where(a => a.user == user).FirstOrDefault();
            if (_user != null)
                return _user.following;
            else
                return new string[0];
        }

    }
}